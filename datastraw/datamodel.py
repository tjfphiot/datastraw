# Timothy Franke  --  27 Apr 2018
# (C) Parker Hannifin Corporation

import adal
import azure.datalake.store.lib

import getpass


# DATA ORGANIZATION -------------------------------------------------
        
class Datalake:
    def __init__( self, store_name, machine_data_root ):
        self.store_name = store_name
        self.machine_data_root = machine_data_root 
    def list_accounts( self ):
        return []
    def account( self, pts_account_name ):
        return Account( self.store_name, self.machine_data_root, pts_account_name )
        
class Account:
    def __init__( self, store_name, machine_data_root, pts_account_name ):
        self.store_name = store_name
        self.machine_data_root = machine_data_root
        self.pts_account_name = pts_account_name
    def list_products( self ):
        return []
    def product( self, product_name ):
        return Product( self.store_name, self.machine_data_root, self.pts_account_name, self.product_name )

class Product:
    def __init__( self, store_name, machine_data_root, pts_account_name, product_name ):
        self.store_name = store_name
        self.machine_data_root = machine_data_root
        self.pts_account_name = pts_account_name
        self.product_name = product_name
    def list_devices( self ):
        return []
    def device( self, device_name ):
        return Device( self.store_name, self.machine_data_root, self.pts_account_name, self.product_name, self.device_name )

class Device:
    def __init__( self, store_name, machine_data_root, pts_account_name, product_name, device_name ):
        self.store_name = store_name
        self.machine_data_root = machine_data_root
        self.pts_account_name = pts_account_name
        self.product_name = product_name
        self.device_name = device_name
    def list_signals( self ):
        return []
    def signal( self, signal_name ):
        return Signal( self.store_name, self.machine_data_root, self.pts_account_name, self.product_name, self.device_name, self.signal_name )

class Signal:
    def __init__( self, store_name, machine_data_root, pts_account_name, product_name, device_name, signal_name ):
        self.store_name = store_name
        self.machine_data_root = machine_data_root
        self.pts_account_name = pts_account_name
        self.product_name = product_name
        self.device_name = device_name
        self.signal_name = signal_name

# Some common datalakes:
parker_ltds = Datalake( 'parkervomltds', 'voice-of-the-machine/v1' )
parker_ltds_dev = Datalake( 'parkervomltdsdev', 'voice-of-the-machine/v1' )
parker_beta = Datalake( 'parkercorpiotbeta', 'voice-of-the-machine/beta-v1' )

# Example: FTS CTV device data from teststand (PTS-ID: 9RPOLVE8):
# parker_ltds.account('K97812').product('3_inch_pump').device('9RPOLVE8')

# TOKENS ------------------------------------------------------------

max_login_attempts = 3

def prompt_for_credentials( username=None ):
    """Prompts the user for their login credentials in an interactive way.
    Returns an Azure token if successful.  By setting the username paramter,
    forces the user to login on that account."""
    # Banner greeting
    print( 'Welcome to the Parker Hannifin Azure system.' )
    print( 'Please supply your Parker active directory credentials.' )
    print( '(Typically, this is your <clock-number>@parker.com.)' )
    # Try to get a successful login--limit login attempts
    for n in range( 0, max_login_attempts ):   
        # Prompt for username, unless it has been specified...
        if username == None:
            supplied_username = input( 'AD Username: ' )
            if supplied_username == '':
                print( 'Empty username, aborting...' )
                raise ValueError( 'User aborted login' )
        else:
            supplied_username = username
            print( 'username: {}'.format( supplied_username ) )
        # Prompt for the password
        supplied_password = getpass.getpass( 'AD Password: ' )
        if supplied_password == '':
                print( 'Empty password, aborting...' )
                raise ValueError( 'User aborted login' )
        try:
            token = azure.datalake.store.lib.auth( tenant_id='parkercorp.onmicrosoft.com', username=supplied_username, password=supplied_password )
            return token
        except adal.AdalError:
            print( 'Invalid credentials.' )
            continue
        except OSError:
            # Todo: retry, maybe?
            print( 'Network error.  Aborting...' )
            raise ValueError( 'Network unreachable' )
    # If we're here, it means we failed to login after the maximum number of tries.
    print( 'Maximum number of login attempts exceeded.  Login aborting...' )
    raise ValueError( 'Invalid login credentials' )

def save_token( token, filename ):
    """Save the token to file."""
    with open( filename, 'wb' ) as f:
        pickle.dump( token, f )
        
def load_token( filename ):
    """Load a saved token from file."""
    with open( filename, 'rb' ) as f:
        return pickle.load( f )


        
# CONNECTIONS -------------------------------------------------------



