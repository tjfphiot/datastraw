# Data Straw

The purpose of this library is to allow sequential access to datalake device data
without the need for big data processing tools.  The library manages retrieval of
datalake files in the background, providing an iterator interface to the data 
streams.  By minimizing how many files need to be open at a time, the total memory
usage of the computer running analytics is kept to a reasonable level.

The primary audience for this library will be analysts interested in data
exploration of IoT data.  It's unclear whether this code will find a place in the 
final data pipeline, or if it will be replaced by more sophisticated tooling.

# Datalakes and Device Data

# Connecting

## Authorization Tokens

Access to the datalake is controlled by authorization tokens.  Parker uses your 
Active Directory credentials to control access to datalake instances.  

We provide an interactive command to prompt the user for their
access credentials.  This is the preferred way when using Notebooks, since it keeps
the user's password hidden.

```python
token = prompt_for_credentials()
```
You can also supply the username
```python
token = prompt_for_credentials( username='123456@parker.com' )
```

Once a token is obtained, we provide a way to persist it to disk, and retrieve it to
be used later.
```python
save_token( token, 'my-creds.tok' )
token = load_token( 'my-creds.tok' )
```
Be mindful of where the token file is stored--it gives anyone full access to your 
account!


# Installation


